import { renderList } from "../controllers/productAdmin.js";
import {
  layThongTintuForm,
  showThongTinLenForm,
} from "../controllers/listPhone.js";
import { validator } from "../models/validator.js";
const BARE_URL = `https://630450110de3cd918b44eb00.mockapi.io`;

let listSp = [];

let batLoading = () => {
  document.getElementById("loadingSp").style.display = "flex";
};
let tatLoading = () => {
  document.getElementById("loadingSp").style.display = "none";
};

let renderListSp = () => {
  batLoading();
  axios({
    url: `${BARE_URL}/Products`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      listSp = res.data;
      renderList(listSp);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};
renderListSp();

let themSp = () => {
  let dataForm = layThongTintuForm();

  // ma
  let isValid = validator.kiemTraRong(dataForm.id, "tbMa");
  // ten
  isValid = isValid & validator.kiemTraRong(dataForm.name, "tbTen");
  // gia
  isValid =
    isValid &
    (validator.kiemTraRong(dataForm.price, "tbGia") &&
      validator.kiemTraGia(dataForm.price, "tbGia"));
  // hinhAnh
  isValid =
    isValid &
    (validator.kiemTraRong(dataForm.img, "tbHinhAnh") &&
      validator.kiemTraHinhAnh(dataForm.img, "tbHinhAnh"));

  // moTa
  isValid = isValid & validator.kiemTraRong(dataForm.desc, "tbMoTa");

  // hang
  isValid = isValid & validator.kiemTraRong(dataForm.type, "tbHang");

  if (isValid == false) {
    return;
  }
  batLoading();
  axios({
    url: `${BARE_URL}/Products`,
    method: "POST",
    data: dataForm,
  })
    .then((res) => {
      tatLoading();
      renderListSp();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

let xoaSp = (id) => {
  batLoading();
  axios({
    url: `${BARE_URL}/Products/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      tatLoading();
      renderListSp();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

let suaSp = (id) => {
  batLoading();
  axios({
    url: `${BARE_URL}/Products/${id}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

let capNhatSp = () => {
  let dataForm = layThongTintuForm();
  batLoading();
  axios({
    url: `${BARE_URL}/Products/${dataForm.ma}`,
    method: "PUT",
    data: dataForm,
  })
    .then((res) => {
      console.log("res: ", res.data);
      tatLoading();
      renderListSp();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

// validator

window.suaSp = suaSp;
window.capNhatSp = capNhatSp;
window.xoaSp = xoaSp;
window.themSp = themSp;
