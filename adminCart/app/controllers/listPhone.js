import { sanPham } from "./productAdmin.js";

export let layThongTintuForm = () => {
  let id = document.getElementById("MaSP").value.trim();
  let name = document.getElementById("TenSP").value.trim();
  let price = document.getElementById("GiaSP").value.trim();
  let img = document.getElementById("HinhSP").value.trim();
  let desc = document.getElementById("MoTaSp").value.trim();
  let type = document.getElementById("HangSp").value.trim();
  let sp = new sanPham(id, name, price, img, desc, type);
  return sp;
};

export let showThongTinLenForm = (arr) => {
  document.getElementById("MaSP").value = arr.id;
  document.getElementById("TenSP").value = arr.name;
  document.getElementById("GiaSP").value = arr.price;
  document.getElementById("HinhSP").value = arr.img;
  document.getElementById("MoTaSp").value = arr.desc;
  document.getElementById("HangSp").value = arr.type;
};
