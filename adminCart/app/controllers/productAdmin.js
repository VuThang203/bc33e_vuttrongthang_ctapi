export let renderList = (list) => {
  let contentHTML = ``;
  list.forEach((item) => {
    let content = `
    <tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.price}$</td>
    <td><img width="60px" src="${item.img}" alt="" /></td>
    <td>${item.desc}</td>
    <td>${item.type}</td>
    <td>
    <button onclick="xoaSp(${item.id})" class= "mb-3 btn btn-warning">Xoá</button>
    <button data-toggle="modal" data-target="#myModal" onclick="suaSp(${item.id})" class= "mb-3 btn btn-primary">Sửa</button>
    </td>
    </tr>
    `;
    contentHTML += content;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

export class sanPham {
  constructor(id, name, price, img, desc, type) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.img = img;
    this.desc = desc;
    this.type = type;
  }
}
