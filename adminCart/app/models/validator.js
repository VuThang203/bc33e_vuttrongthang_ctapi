export let validator = {
  kiemTraRong: function (valueInput, idError) {
    if (valueInput == "") {
      document.getElementById(idError).innerText = "Không được để rỗng!";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraGia: function (valueInput, idError) {
    let ktGia = /^\d+(,\d{1,2})?$/;
    if (ktGia.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Nhập sai định dạng";
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },
  kiemTraHinhAnh: function (valueInput, idError) {
    let ktHinhAnh = /.*\.(gif|jpe?g|bmp|png)$/;
    if (ktHinhAnh.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Hình phải có dạng: gif, jpe, jpg, bmp, png.";
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },
};
