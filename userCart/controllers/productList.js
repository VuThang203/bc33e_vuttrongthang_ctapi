export let renderListPhone = (list) => {
  let contentHTML = ``;
  list.forEach((item) => {
    let content = `<div class="card mb-5 ml-5" style="width: 18rem;">
    <div  class="card-top"><img src=${item.img} alt="..."></div>
    <div class="card-body">
      <h5 class="card-title ">${item.name} <br> <span>(${
      item.desc
    })</span> </h5>
      <p class="card-text">
       <span><i class="fa fa-mobile-alt"></i> : ${item.screen}</span> <br>
      <span><i class="fa fa-camera"></i> : trước ${item.frontCamera}, sau : ${
      item.backCamera
    }
      </span>
      </p>
      <div class="row footer_card">
      <span class="text-danger h3 col-6">${(
        item.price * 1
      ).toLocaleString()} $</span>
      <span class="offset-2 col-3" >
      <a  onclick="addItem('${
        item.id
      }')"  class="btn btn-primary"><i class="fa fa-cart-plus" ></i></a>
      </span>
      </div>
    </div>
    </div>`;
    contentHTML += content;
  });
  document.getElementById("renderList").innerHTML = contentHTML;
};

export let renderCart = (list) => {
  let contentHTML = ``;
  list.forEach((item) => {
    let content = `
    <div>Sản phẩm ${item.product.id}</div>
    <div class="row content__cart">
    <img class="cartInImg" src="${item.product.img}" alt="" />
    <div class="text__cart">
    <div>Product: ${item.product.name}</div>
    <div>Quantity: ${item.quantity}</div>
    <div>Price: $${item.product.price * 1 * item.quantity}</div>
    </div>
    <div class="button__cart">
    <button onclick="tangSp('${
      item.product.id
    }')" class="btn btn-warning ">Tăng</button>
    <button onclick="giamSp('${
      item.product.id
    }')" class="btn btn-primary mr-3 ml-3">Giảm</button>
    <button onclick="xoaSp('${
      item.product.id
    }')" class="btn btn-success">Xoá</button>
    </div>
    </div>
   
    `;
    contentHTML += content;
  });
  document.getElementById("showSP").innerHTML = contentHTML;
};
