// export class listPhoneAll {
//   constructor(name, price, screen, backCamera, frontCamera, img, desc, type) {
//     this.name = name;
//     this.price = price;
//     this.screen = screen;
//     this.backCamera = backCamera;
//     this.frontCamera = frontCamera;
//     this.img = img;
//     this.desc = desc;
//     this.type = type;
//   }
// }

export class cartItem {
  constructor(product) {
    this.product = product;
    this.quantity = 1;
  }
}

export let timKiem = (id, arr) => {
  let index = arr.findIndex((item) => item.product.id == id);
  return index;
};

export let thanhToan = (list) => {
  let total = 0;
  list.forEach((item) => {
    let sumItem = item.product.price * 1 * item.quantity;
    total += sumItem;
  });
  document.getElementById(
    "tinhTienSP"
  ).innerHTML = `Số tiền bạn cần phải trả là: ${total}$`;
};
