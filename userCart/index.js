import { renderListPhone } from "./controllers/productList.js";
import { cartItem } from "./models/listPhone.js";
import { timKiem, thanhToan } from "./models/listPhone.js";
import { renderCart } from "./controllers/productList.js";
const BARE_URL = "https://630450110de3cd918b44eb00.mockapi.io";

let batLoading = () => {
  document.getElementById("loadingSp").style.display = "flex";
};

let tatLoading = () => {
  document.getElementById("loadingSp").style.display = "none";
};

let listSp = [];
let cart = [];

// Lưu localstorage

let saveLocalStorage = () => {
  let cartJson = JSON.stringify(cart);
  localStorage.setItem("CART", cartJson);
};

// Lấy xuống
let cartLocal = localStorage.getItem("CART");
if (JSON.parse(cartLocal)) {
  cart = JSON.parse(cartLocal);
  console.log("data: ", cart);
  renderCart(cart);
  // thanhToan(cart);
}

let renderTableService = () => {
  batLoading();
  axios({
    url: `${BARE_URL}/Products`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      // console.log("res: ", res.data);
      listSp = res.data;
      searchSP();
      renderListPhone(listSp);
      saveLocalStorage();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};
renderTableService();

let addItem = (id) => {
  batLoading();
  axios({
    url: `${BARE_URL}/Products/${id}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      let cartI = new cartItem(res.data);
      let index = timKiem(id, cart);
      if (index == -1) {
        cart.push(cartI);
      } else {
        cart[index].quantity += 1;
      }
      renderCart(cart);
      thanhToan(cart);
      // removeAllList(cart);
      saveLocalStorage();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

let tangSp = (id) => {
  let index = timKiem(id, cart);
  if (index !== -1) {
    cart[index].quantity += 1;
  }

  if (cart[index].quantity <= 0) {
    return;
  }
  renderCart(cart);
  thanhToan(cart);
  saveLocalStorage();
};

let giamSp = (id) => {
  let index = timKiem(id, cart);
  if (cart[index].quantity >= 1 && index !== -1) {
    cart[index].quantity -= 1;
  }
  if (cart[index].quantity <= 0) {
    cart.splice(index, 1);
  }

  renderCart(cart);
  thanhToan(cart);
  saveLocalStorage();
};

let xoaSp = (id) => {
  let index = timKiem(id, cart);
  if (index !== -1) {
    cart.splice(index, 1);
  }
  renderCart(cart);
  thanhToan(cart);
  saveLocalStorage();
};

let removeAllList = () => {
  if (true) {
    cart.splice(0, cart.length);
  }

  renderCart(cart);
  saveLocalStorage();
};

let deleteALl = () => {
  if (true) {
    cart.splice(0, cart.length);
  }

  renderCart(cart);
  saveLocalStorage();
};

let searchSP = () => {
  let giaTriTim = document.getElementById("searchName").value;
  giaTriTim.toLowerCase();
  let danhSachTim = [];
  listSp.forEach((item) => {
    let soSanh = item.type;
    soSanh.toLowerCase();
    if (soSanh.toLowerCase().includes(giaTriTim.toLowerCase())) {
      danhSachTim.push(item);
    }
  });
  renderListPhone(danhSachTim);
  saveLocalStorage();
};

function resetForm() {
  document.getElementById("showSP").reset();
}

window.deleteALl = deleteALl;
window.removeAllList = removeAllList;
window.xoaSp = xoaSp;
window.giamSp = giamSp;
window.tangSp = tangSp;
window.searchSP = searchSP;
window.addItem = addItem;
